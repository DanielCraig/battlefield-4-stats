const bf4 = {
    onlinePlayers: window.onload = function () {

        let players = document.getElementById('playersOnline')

        fetch("https://api.bf4stats.com/api/onlinePlayers").then(response => response.json()).then(data => {

            let platforms = {
                pc: data.pc.count,
                ps3: data.ps3.count,
                ps4: data.ps4.count,
                xbox360: data.xbox.count,
                xone: data.xone.count
            }

            let total = platforms.pc + platforms.ps3 + platforms.ps4 + platforms.xbox360 + platforms.xone

            players.innerHTML = '<p class="uk-text-success">Players online: ' + total + '</p>'
        })
    },

    playerSearch: function () {
        let searchButton = document.getElementById('searchButton')
        searchButton.addEventListener('click', playerQuery)

        function playerQuery() {
            let searchBox = document.getElementById('searchBox').value
            let platform = document.getElementById('platformSelector').value
            fetch('https://api.bf4stats.com/api/playerInfo?plat=' + platform + '&name=' + searchBox + '&output=json').then(response => response.json()).then(data => {
                //console.log('Player data is: ', data)
                let playerdata = {
                    username: data.player.uName,
                    lastOnline: data.player.lastDay,
                    score: data.player.score,
                    rank: data.player.rank.nr,
                    rankname: data.player.rank.name
                }

                let detailedscore = {
                    kills: data.stats.kills,
                    deaths: data.stats.deaths,
                    dogtagstaken: data.stats.dogtagsTaken,
                    flagCaptures: data.stats.flagCaptures,
                    hs: data.stats.headshots,
                    killAssists: data.stats.killAssists,
                    longestHeadshot: data.stats.longestHeadshot,
                    shotsFired: data.stats.shotsFired,
                    shotsHit: data.stats.shotsHit,
                    skill: data.stats.skill,
                    assaultscore: data.stats.kits.assault.score,
                    engineerscore: data.stats.kits.engineer.score,
                    reconscore: data.stats.kits.recon.score,
                    supportscore: data.stats.kits.support.score
                }


                document.getElementById('mainAccordion').innerHTML =
                    '<li class="uk uk-open">' +
                    '<a class="uk-accordion-title">Player Info</a>' +
                    '<div class="uk-accordion-content">' +
                    '<div class="uk-card uk-card-default uk-width-1-2@m uk-card-hover">' +
                    '<div class="uk-card-header">' +
                    '<div class="uk-grid-small uk-flex-middle" uk-grid>' +
                    '<div class="uk-width-auto">' +
                    '<img class="uk-border-circle" width="50" height="50"' +
                    'src="https://images-na.ssl-images-amazon.com/images/I/51GOnGYUylL.jpg">' +
                    '</div>' +
                    ' <div class="uk-width-expand">' +
                    '<h3 class="uk-card-title uk-margin-remove-bottom">' + playerdata.username + '</h3>' +
                    '<p class="uk-text-meta uk-margin-remove-top">' + 'Rank: ' + playerdata.rank + ' ' + playerdata.rankname + '</p>' +
                    '</div>' +
                    '</div>' +
                    '</div>' +
                    '<div class="uk-card-body">' +
                    '<p><strong>Player score:</strong> ' + playerdata.score + ' points</p>' +
                    '<p><strong>Last time seen:</strong> ' + playerdata.lastOnline + '</p>' +
                    '</div>' +
                    '</div >' +
                    '</li>' +
                    '</div>' +
                    '<li class="uk">' +
                    '<a class="uk-accordion-title">Detailed Stats</a>' +
                    '<div class="uk-accordion-content">' +
                    '<div class="uk-card uk-card-default uk-width-1-2@m uk-card-hover">' +
                    '<div class="uk-card-header">' +
                    '<div class="uk-grid-small uk-flex-middle" uk-grid>' +
                    '<div class="uk-width-auto">' +
                    '<img class="uk-border-circle" width="50" height="50"' +
                    'src="https://images-na.ssl-images-amazon.com/images/I/51GOnGYUylL.jpg">' +
                    '</div>' +
                    ' <div class="uk-width-expand">' +
                    '<h3 class="uk-card-title uk-margin-remove-bottom">' + playerdata.username + '</h3>' +
                    '<p class="uk-text-meta uk-margin-remove-top">' + 'Rank: ' + playerdata.rank + ' ' + playerdata.rankname + '</p>' +
                    '</div>' +
                    '</div>' +
                    '</div>' +
                    '<div class="uk-card-body">' +
                    '<div class="uk-grid">' +
                    '<div>' +
                    '<p><strong>Kills:</strong> ' + detailedscore.kills + '</p>' +
                    '<p><strong>Deaths:</strong> ' + detailedscore.deaths + '</p>' +
                    '<p><strong>Kill Assists:</strong> ' + detailedscore.killAssists + '</p>' +
                    '</div>' +
                    '<div>' +
                    '<p><strong>Shots Fired:</strong> ' + detailedscore.shotsFired + '</p>' +
                    '<p><strong>Shots Hit:</strong> ' + detailedscore.shotsHit + '</p>' +
                    '<p><strong>Headshots:</strong> ' + detailedscore.hs + '</p>' +
                    '</div>' +
                    '</div>' +
                    '</div>' +
                    '</div >' +
                    '</li>' +
                    '</div>' +
                    '<li class="uk">' +
                    '<a class="uk-accordion-title">Score Info</a>' +
                    '<div class="uk-accordion-content">' +
                    '<div class="uk-card uk-card-default uk-width-1-2@m uk-card-hover">' +
                    '<div class="uk-card-header">' +
                    '<div class="uk-grid-small uk-flex-middle" uk-grid>' +
                    '<div class="uk-width-auto">' +
                    '<img class="uk-border-circle" width="50" height="50"' +
                    'src="https://images-na.ssl-images-amazon.com/images/I/51GOnGYUylL.jpg">' +
                    '</div>' +
                    ' <div class="uk-width-expand">' +
                    '<h3 class="uk-card-title uk-margin-remove-bottom">' + playerdata.username + '</h3>' +
                    '<p class="uk-text-meta uk-margin-remove-top">' + 'Rank: ' + playerdata.rank + ' ' + playerdata.rankname + '</p>' +
                    '</div>' +
                    '</div>' +
                    '</div>' +
                    '<div class="uk-card-body">' +
                    '<div class="uk-grid">' +
                    '<div>' +
                    '<p><strong>Assault Score:</strong> ' + detailedscore.assaultscore + '</p>' +
                    '<p><strong>Recon Score:</strong> ' + detailedscore.reconscore + '</p>' +
                    '<p><strong>Engineer Score:</strong> ' + detailedscore.engineerscore + '</p>' +
                    '</div>' +
                    '<div>' +
                    '<p><strong>Support Score:</strong> ' + detailedscore.supportscore + '</p>' +
                    '<p><strong>Dogtags taken:</strong> ' + detailedscore.dogtagstaken + '</p>' +
                    '<p><strong>Longest Headshot:</strong> ' + detailedscore.longestHeadshot + ' Meters</p>' +
                    '</div>' +
                    '</div>' +
                    '</div>' +
                    '</div >' +
                    '</li>' +
                    '</div>'

            })
        }
    }
}
bf4.playerSearch()